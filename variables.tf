variable "api_id" {
  description = "The ID of the API as part of which this method is deployed."
}

variable "api_name" {
  description = "The name of the API for which this method/function is defined. It is used as a prefix of the Lambda function's name."
}

variable "resource_id" {
  description = "The ID of the API resource to which this method is associated."
}

variable "resource_path" {
  description = "The full path of the API resource."
}

variable "fn_stages" {
  type = list(object({
    stage_name  = string,
    fn_lang     = optional(string)
    fn_arch     = optional(string)
    fn_mem      = optional(number)
    fn_timeout  = optional(number)
    fn_env_vars = optional(map(string))
  }))
  description = <<-DOC

    Specify names of all the deployment stages for which to define Lambda handlers as
    stage-definition objects where each stage must have at least a stage name via `stage_name`.

    Additional optional config properties are: `fn_mem`, `fm_timeout`, `fn_lang`. If specified they
    override the module-level parameter with the same name.

    `fn_stages = [`
    `  { stage_name = "v1", },`
    `  { stage_name = "v2"`
    `    fn_lang = "node16",`
    `    fn_mem = 256,`
    `    fn_timeout = 1 } ]`
  DOC
}

variable "fn_name_prefix" {
  description = "The name prefix of the Lambda functions, which will be combined with the stage name as suffix to define a Lambda function per stage in the list of stage names."
}

variable "fn_arch" {
  description = "The runtime CPU architecture for the function, one of: `x86_64`, `arm64`."
  validation {
    condition     = contains(["x86_64", "arm64"], var.fn_arch)
    error_message = "The architecture must be one that is supported."
  }
  default = "x86_64"
}

variable "fn_lang" {
  description = "The runtime language for the function, one of: `node16`, `node18`, `node20`, `ruby3_2`, `ruby3_3`, `go1`."
  validation {
    condition     = contains(["node16", "node18", "node20", "ruby3_2", "ruby3_3", "go1"], var.fn_lang)
    error_message = "The language must be one that is supported."
  }
  default = "node16"
}

variable "fn_timeout" {
  description = "The maximum time in seconds the function can run for. Default is `3s`."
  type        = number
  default     = 3
}

variable "fn_mem" {
  description = "The amount of memory in MB for the Lambda function. Defaults to `128`."
  type        = number
  validation {
    condition     = var.fn_mem >= 128 && var.fn_mem % 64 == 0
    error_message = "The memory must be at least 128 and specified in 64MB increments."
  }
  default = 128
}

variable "http_method" {
  description = "The API method's HTTP method type. E.g. `GET`, `POST` etc."
  validation {
    condition = contains(
      ["GET", "POST", "PUT", "DELETE", "HEAD", "OPTIONS", "PATCH"],
      var.http_method
    )
    error_message = "The HTTP method must be valid and supported."
  }
}

variable "lambda_role_arn" {
  description = "The ARN for a lambda execution role. Use the `api_lambda_role` module to create one."
}

# variable "auth_id" {
#   description = "The custom authorizer ID; accessible from the `api_lambda_auth` module outputs. If not specified, no authorization is used."
#   default     = null
# }

variable "key_arn" {
  description = "The KMS key ARN for Lambda to use instead of the default service key provided by AWS."
  default     = null
}

variable "fn_env_vars" {
  description = "Optional map of environment variables to pass through to the Lambda function."
  type        = map(string)
  default     = {}
}

variable "fn_code_bucket_id" {
  description = "Optional bucket ID to use to store/upload function code; if not specified, functions are uploaded directly"
  type        = string
  default     = null
}

variable "fn_code_use_bucket" {
  description = "Set to `true` to use the specific bucket to host the code for all the Lambda functions"
  type        = bool
  default     = false
}

variable "tags" {
  description = "Map of tags to attach to any resources in this module that support tags. Defaults to `{}`."
  type        = map(string)
  default     = {}
}

variable "use_cors_response_template" {
  type        = string
  description = "Velocity response teplate to use if CORS is enabled."
  default     = ""
}
