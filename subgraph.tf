
locals {
  dot_method = "${var.fn_name_prefix}_${md5("${var.api_id}/${var.resource_id}/${var.http_method}")}"
  awshash    = md5(data.aws_caller_identity.current.id)
  dot_functions = { for s, f in aws_lambda_function.this :
    "${var.fn_name_prefix}_${md5(f.arn)}" => merge({ stage_name = s }, f, )
  }
}

output "graphviz" {
  description = "Description of the module instance for use with Graphviz `dot`."
  value = {
    subgraph = <<EOT
      subgraph { 
        node [shape=record]
        ${local.dot_method} [label="Method | ${var.fn_name_prefix} }"]
        { ${join(", ", keys(local.dot_functions))} } -> ${local.dot_method} [dir=back, style=dashed, label="invoke"]
      }

      ${join("\n", [for n, f in local.dot_functions : "subgraph cluster_${f.stage_name}_${local.awshash} { label = \"Stage ${f.stage_name}\"; ${n} [label= \"Function | ${f.stage_name}_${var.fn_name_prefix}\"] }"])}


    EOT
    nodes = {
      this      = local.dot_method
      functions = { for n, f in local.dot_functions : f.stage_name => n }
    }
  }
}
