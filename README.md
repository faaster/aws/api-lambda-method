# FaaSTer AWS API Staged Lambda Method Module

This is a Terraform module that can be used to define and create one AWS Lambda-backed API Gateway method with multi-stage support.

> Module works; documentation is still a work in progress.

- [Changelog](#changelog)
- [Usage](#usage)
  - [One Method, One Stage](#one-method-one-stage)
  - [Multiple Methods](#multiple-methods)
- [Module Reference](#module-reference)
  - [Requirements](#requirements)
  - [Providers](#providers)
  - [Modules](#modules)
  - [Resources](#resources)
  - [Inputs](#inputs)
  - [Outputs](#outputs)

## Changelog

See the [repository tags list](https://gitlab.com/faaster/aws/api-lambda-method/-/tags) for the changelog.

## Usage

### One Method, One Stage

```hcl
# assumes the following resources
#    - resource aws_api_gateway_rest_api "this" { ... }
#    - resource aws_iam_role "lambda" { ... }

module "my_GET_message" {
  source        = "git::https://gitlab.com/faaster/aws/api-lambda-method.git"
  api_id        = aws_api_gateway_rest_api.this.id
  fn_stages     = [
      {
        stage_name = "v1",
      }]
  resource_id    = aws_api_gateway_rest_api.this.root_resource_id   # at the root of the API
  resource_path  = "/"
  http_method    = "GET"
  fn_name_prefix = "my_get_message"
  fn_timeout     = 1000
  fn_mem         = 256
  fn_lang        = "node12"
  env_vars       = {
      DEPLOY_ENV = var.environment
  }
  lambda_role    = aws_iam_role.lambda
}

```

### Multiple Methods

This module works well with the following modules:
* [`faaster/aws/api-resource-tree`](https://gitlab.com/faaster/aws/api-resource-tree) using `for_each`.
* [`faaster/aws/lambda-role](https://gitlab.com/faaster/aws/lambda-role)


```hcl
locals {
  # Convert the flat path method list into a map using a nice key
  methods_map = { for meth in module.api_paths.methods_list :
                            "${meth.method}/${meth.fullpath}" => meth }
}

module "api_methods" {
  source        = "git::https://gitlab.com/faaster/aws/api-lambda-method.git"
  for_each      = local.methods_map
  api_id        = aws_api_gateway_rest_api.this.id
  fn_stages     = var.stages
  resource_id    = each.value.api_res_id
  resource_path  = each.value.fullpath
  http_method    = each.value.method
  fn_name_prefix = each.value.fn_name
  fn_timeout     = lookup(each.value, "fn_timeout", 1000)
  fn_mem         = lookup(each.value, "fn_mem", 256)
  fn_lang        = lookup(each.value, "fn_lang", "node12")
  env_vars       = lookup(each.value, "env_vars", {})
  lambda_role    = module.lambda_role.this
}
```

## Module Reference

### Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.14 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.30.0 |

### Providers

| Name | Version |
|------|---------|
| <a name="provider_archive"></a> [archive](#provider\_archive) | n/a |
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 3.30.0 |
| <a name="provider_local"></a> [local](#provider\_local) | n/a |

### Modules

No modules.

### Resources

| Name | Type |
|------|------|
| [aws_api_gateway_integration.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration) | resource |
| [aws_api_gateway_integration_response.this_200](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration_response) | resource |
| [aws_api_gateway_method.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method) | resource |
| [aws_api_gateway_method_response.this_200](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method_response) | resource |
| [aws_lambda_function.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_function) | resource |
| [aws_lambda_permission.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_permission) | resource |
| [aws_s3_bucket_object.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_object) | resource |
| [local_file.lambda](https://registry.terraform.io/providers/hashicorp/local/latest/docs/resources/file) | resource |
| [archive_file.lambda](https://registry.terraform.io/providers/hashicorp/archive/latest/docs/data-sources/file) | data source |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |

### Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_api_id"></a> [api\_id](#input\_api\_id) | The ID of the API as part of which this method is deployed. | `any` | n/a | yes |
| <a name="input_api_name"></a> [api\_name](#input\_api\_name) | The name of the API for which this method/function is defined. It is used as a prefix of the Lambda function's name. | `any` | n/a | yes |
| <a name="input_fn_arch"></a> [fn\_arch](#input\_fn\_arch) | The runtime CPU architecture for the function, one of: `x86_64`, `arm64`. | `string` | `"x86_64"` | no |
| <a name="input_fn_code_bucket_id"></a> [fn\_code\_bucket\_id](#input\_fn\_code\_bucket\_id) | Optional bucket ID to use to store/upload function code; if not specified, functions are uploaded directly | `string` | `null` | no |
| <a name="input_fn_code_use_bucket"></a> [fn\_code\_use\_bucket](#input\_fn\_code\_use\_bucket) | Set to `true` to use the specific bucket to host the code for all the Lambda functions | `bool` | `false` | no |
| <a name="input_fn_env_vars"></a> [fn\_env\_vars](#input\_fn\_env\_vars) | Optional map of environment variables to pass through to the Lambda function. | `map(string)` | `{}` | no |
| <a name="input_fn_lang"></a> [fn\_lang](#input\_fn\_lang) | The runtime language for the function, one of: `node16`, `node18`, `node20`, `ruby3_2`, `ruby3_3`, `go1`. | `string` | `"node16"` | no |
| <a name="input_fn_mem"></a> [fn\_mem](#input\_fn\_mem) | The amount of memory in MB for the Lambda function. Defaults to `128`. | `number` | `128` | no |
| <a name="input_fn_name_prefix"></a> [fn\_name\_prefix](#input\_fn\_name\_prefix) | The name prefix of the Lambda functions, which will be combined with the stage name as suffix to define a Lambda function per stage in the list of stage names. | `any` | n/a | yes |
| <a name="input_fn_stages"></a> [fn\_stages](#input\_fn\_stages) | Specify names of all the deployment stages for which to define Lambda handlers as<br>stage-definition objects where each stage must have at least a stage name via `stage_name`.<br><br>Additional optional config properties are: `fn_mem`, `fm_timeout`, `fn_lang`. If specified they<br>override the module-level parameter with the same name.<br><br>`fn_stages = [`<br>`  { stage_name = "v1", },`<br>`  { stage_name = "v2"`<br>`    fn_lang = "node16",`<br>`    fn_mem = 256,`<br>`    fn_timeout = 1 } ]` | <pre>list(object({<br>    stage_name  = string,<br>    fn_lang     = optional(string)<br>    fn_arch     = optional(string)<br>    fn_mem      = optional(number)<br>    fn_timeout  = optional(number)<br>    fn_env_vars = optional(map(string))<br>  }))</pre> | n/a | yes |
| <a name="input_fn_timeout"></a> [fn\_timeout](#input\_fn\_timeout) | The maximum time in seconds the function can run for. Default is `3s`. | `number` | `3` | no |
| <a name="input_http_method"></a> [http\_method](#input\_http\_method) | The API method's HTTP method type. E.g. `GET`, `POST` etc. | `any` | n/a | yes |
| <a name="input_key_arn"></a> [key\_arn](#input\_key\_arn) | The KMS key ARN for Lambda to use instead of the default service key provided by AWS. | `any` | `null` | no |
| <a name="input_lambda_role_arn"></a> [lambda\_role\_arn](#input\_lambda\_role\_arn) | The ARN for a lambda execution role. Use the `api_lambda_role` module to create one. | `any` | n/a | yes |
| <a name="input_resource_id"></a> [resource\_id](#input\_resource\_id) | The ID of the API resource to which this method is associated. | `any` | n/a | yes |
| <a name="input_resource_path"></a> [resource\_path](#input\_resource\_path) | The full path of the API resource. | `any` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | Map of tags to attach to any resources in this module that support tags. Defaults to `{}`. | `map(string)` | `{}` | no |
| <a name="input_use_cors_response_template"></a> [use\_cors\_response\_template](#input\_use\_cors\_response\_template) | Velocity response teplate to use if CORS is enabled. | `string` | `""` | no |

### Outputs

| Name | Description |
|------|-------------|
| <a name="output_functions"></a> [functions](#output\_functions) | The list of per-stage Lambda function information. |
| <a name="output_graphviz"></a> [graphviz](#output\_graphviz) | Description of the module instance for use with Graphviz `dot`. |
| <a name="output_trigger_hash"></a> [trigger\_hash](#output\_trigger\_hash) | Hash of aspects of the method spec that if changed should be used to trigger re-deployment. |
| <a name="output_type"></a> [type](#output\_type) | n/a |
