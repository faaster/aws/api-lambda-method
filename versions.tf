
terraform {
  required_version = ">= 0.14"
}

terraform {
  required_providers {
    aws = {
      version = ">= 3.30.0"
      source  = "hashicorp/aws"
    }
  }
}
