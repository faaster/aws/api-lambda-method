data "aws_region" "current" {
}

data "aws_caller_identity" "current" {
}

locals {
  //
  // the runtime map lets us map the language names to
  // supported Lambda runtimes
  runtime_map = {
    node16  = { runtime = "nodejs16.x", base = "node", srcpath = "node", handler = "index.handler", arm64 = true }
    node18  = { runtime = "nodejs18.x", base = "node", srcpath = "node", handler = "index.handler", arm64 = true }
    node20  = { runtime = "nodejs20.x", base = "node", srcpath = "node", handler = "index.handler", arm64 = true }
    go1     = { runtime = "go1.x", base = "go", srcpath = "go/src", handler = "handler", arm64 = false }
    ruby3_2 = { runtime = "ruby3.2", base = "ruby", srcpath = "ruby", handler = "function.handler", arm64 = true }
    ruby3_3 = { runtime = "ruby3.3", base = "ruby", srcpath = "ruby", handler = "function.handler", arm64 = true }
  }

  //
  // the lang name map lets us map the runtime backs to the proper
  // lang name
  lang_name_map = {
    "nodejs16.x" = "node16"
    "nodejs18.x" = "node18"
    "nodejs20.x" = "node20"
    "go1.x"      = "go1"
    "ruby3.2"    = "ruby3_2"
    "ruby3.3"    = "ruby3_3"
  }

  default_env_vars = {
  }

  fn_defaults = {
    fn_mem     = var.fn_mem
    fn_timeout = var.fn_timeout
    fn_lang    = var.fn_lang
    fn_arch    = var.fn_arch
  }

  #
  # Create a map of per-stage function specs by merging each stage's object with
  # the runtime map for the language for each stage.
  #
  fn_stage_map = {
    for s in var.fn_stages : s.stage_name => merge(
      local.fn_defaults,
      s, # stage options, if any, override defaults
      local.runtime_map[s.fn_lang != null ? s.fn_lang : var.fn_lang]
    )
  }
}

#
# Per stage in the list of stage_ids
# Each Lambda function has a naming convention like so:
#
#   "fun_" + STAGE + "_" + NAME
#
# This allows us to define the API method so that it uses the
# stage variable "STAGE_NAME" when defining the Lambda reference
#
# We also supply an env var to each Lambda that identifies the
# stage it's intended for, called
#
#   DEPLOY_STAGE
#
#

resource "local_file" "lambda" {
  for_each             = local.fn_stage_map
  filename             = "./functions/${each.key}/${each.value.srcpath}/${var.fn_name_prefix}/tmp.md"
  file_permission      = "0644" # a+rwx,u-x,g-wx,o-wx
  directory_permission = "0755" # a+rwx,g-w,o-w
  content              = <<-EOT

    ## Temporary

    This file is created to ensure source paths exist when deploying API.

    - Delete it if you wish
    - Don't commit it
    - Will come back every the TF config is applied.

  EOT

}

data "archive_file" "lambda" {
  for_each = local.fn_stage_map
  type     = "zip"

  # Consider per-stage optional config for these Lambda properties before falling back to global config and their defaults
  source_dir  = "./functions/${each.key}/${each.value.srcpath}/${var.fn_name_prefix}"
  output_path = "./functions/fun_${each.key}_${each.value.base}_${var.fn_name_prefix}.zip"

  depends_on = [local_file.lambda]
}

resource "aws_s3_bucket_object" "this" {
  for_each = var.fn_code_use_bucket == true ? local.fn_stage_map : {}
  bucket   = var.fn_code_bucket_id

  key    = "functions/fun_${each.key}_${each.value.base}_${var.fn_name_prefix}.zip"
  source = data.archive_file.lambda[each.key].output_path

  etag = data.archive_file.lambda[each.key].output_md5
}

resource "aws_lambda_function" "this" {
  for_each         = local.fn_stage_map
  function_name    = "${var.api_name}_${each.key}_${var.fn_name_prefix}"
  role             = var.lambda_role_arn
  kms_key_arn      = var.key_arn
  tags             = var.tags
  source_code_hash = data.archive_file.lambda[each.key].output_base64sha256
  # Use S3 bucket if requested; else upload file directly
  filename  = var.fn_code_use_bucket ? null : data.archive_file.lambda[each.key].output_path
  s3_bucket = var.fn_code_use_bucket ? var.fn_code_bucket_id : null
  s3_key    = var.fn_code_use_bucket ? aws_s3_bucket_object.this[each.key].key : null

  # Consider per-stage optional config for these Lambda properties before falling back to global config and their defaults
  handler       = each.value.handler
  runtime       = each.value.runtime
  memory_size   = each.value.fn_mem != null ? each.value.fn_mem : local.fn_defaults.fn_mem
  architectures = [each.value.fn_arch != null ? each.value.fn_arch : local.fn_defaults.fn_arch]
  timeout       = min(900, each.value.fn_timeout != null ? each.value.fn_timeout : local.fn_defaults.fn_timeout)

  environment {
    variables = merge(
      each.value.fn_env_vars != null ? each.value.fn_env_vars : {},
      var.fn_env_vars,
      local.default_env_vars,
      {
        "DEPLOY_STAGE" = each.key
      },
    )
  }
}

resource "aws_lambda_permission" "this" {
  for_each      = local.fn_stage_map
  function_name = aws_lambda_function.this[each.key].function_name
  statement_id  = "AllowExecutionFromApiGateway"
  action        = "lambda:InvokeFunction"
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  source_arn = "arn:aws:execute-api:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:${var.api_id}/*/${aws_api_gateway_method.this.http_method}${var.resource_path}"
}

#
# Define the API method and integration once using stage variable STAGE_NAME to refer to the
# Lambda function, which API gateway will substitute on invocation via the
# stage-specific endpoint
#
resource "aws_api_gateway_method" "this" {
  rest_api_id   = var.api_id
  resource_id   = var.resource_id
  http_method   = var.http_method
  authorization = "NONE"
}

locals {
  fn_staged_var_arn = "arn:aws:lambda:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:function:${var.api_name}_$${stageVariables.STAGE_NAME}_${var.fn_name_prefix}"
}

resource "aws_api_gateway_method_response" "this_200" {
  rest_api_id = var.api_id
  resource_id = var.resource_id
  http_method = aws_api_gateway_integration.this.http_method
  status_code = 200
  response_models = {
    "application/json" = "Empty"
  }
}

resource "aws_api_gateway_integration_response" "this_200" {
  rest_api_id = var.api_id
  resource_id = var.resource_id
  http_method = aws_api_gateway_integration.this.http_method
  status_code = aws_api_gateway_method_response.this_200.status_code

  response_templates = {
    "application/json" = <<-EOF
      ${var.use_cors_response_template}
      $input.json('$.body')
      EOF
  }
}

resource "aws_api_gateway_integration" "this" {
  rest_api_id             = var.api_id
  resource_id             = var.resource_id
  http_method             = aws_api_gateway_method.this.http_method
  type                    = "AWS"
  uri                     = "arn:aws:apigateway:${data.aws_region.current.name}:lambda:path/2015-03-31/functions/${local.fn_staged_var_arn}/invocations"
  integration_http_method = "POST"

  # See http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-mapping-template-reference.html
  # See https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format
  # This template passes through a subset the simple proxy's transformation of input
  request_templates = {
    "application/json" = <<-EOF
      #set($allParams = $input.params())
      {
        "body" : $input.json('$'),
        "resource" : "$context.resourcePath",
        "path" : "$context.path",
        "httpMethod": "$context.httpMethod",
        "header" : {
            #set($params = $allParams.get("header"))
            #foreach($paramName in $params.keySet())
              #set($val = $util.escapeJavaScript($params.get($paramName)).replaceAll("\\'","'"))
              "$paramName" : "$val"
              #if($foreach.hasNext),#end
            #end
        },
        "requestContext" : {
            "accountId" : "$context.identity.accountId",
            "apiId" : "$context.apiId",
            "apiKey" : "$context.identity.apiKey",
            "httpMethod" : "$context.httpMethod",
            "stage" : "$context.stage",
            "requestId" : "$context.requestId",
            "resourceId" : "$context.resourceId",
            "resourcePath" : "$context.resourcePath",
            "identity" : {
              "caller" : "$context.identity.caller",
              "sourceIp" : "$context.identity.sourceIp",
              "user" : "$context.identity.user",
              "userAgent" : "$context.identity.userAgent",
              "userArn" : "$context.identity.userArn"
            }
        },
        "stageVariables" : {
          #foreach($key in $stageVariables.keySet())
            "$key" : "$util.escapeJavaScript($stageVariables.get($key))"
            #if($foreach.hasNext),#end
          #end
        },
        "queryStringParameters" : {
            #set($params = $allParams.get("querystring"))
            #foreach($paramName in $params.keySet())
              #set($val = $util.escapeJavaScript($params.get($paramName)).replaceAll("\\'","'"))
              "$paramName" : "$val"
              #if($foreach.hasNext),#end
            #end
        },
        "pathParameters" : {
            #set($params = $allParams.get("path"))
            #foreach($paramName in $params.keySet())
              #set($val = $util.escapeJavaScript($params.get($paramName)).replaceAll("\\'","'"))
              "$paramName" : "$val"
              #if($foreach.hasNext),#end
            #end
        }
      }
    EOF
  }

  # Use this in the template for trouble-shooting and discovering other data we might need
  # later
  # "original" : {
  #   "params" : {
  #     #foreach($type in $allParams.keySet())
  #       #set($params = $allParams.get($type))
  #       "$type" : {
  #         #foreach($paramName in $params.keySet())
  #           "$paramName" : "$util.escapeJavaScript($params.get($paramName))"
  #           #if($foreach.hasNext),#end
  #         #end
  #       }
  #       #if($foreach.hasNext),#end
  #     #end
  #   },
  #   "dynContext" : {
  #     #foreach($p1 in $context.keySet())
  #       #set($val = $context.get($p1))
  #       #set($typ = $val.getClass().getName())
  #       #if($typ.endsWith("Map"))
  #         "$p1" : {
  #           #foreach($p2 in $val.keySet())
  #             #set($val2 = $val.get($p2))
  #             #set($typ2 = $val2.getClass().getName())
  #             ## "xxxx$p2" : "$typ2",
  #             #if($typ2.endsWith("Map"))
  #               "$p2" : {
  #                   #foreach($p3 in $val2.keySet())
  #                     #set($val3 = $val2.get($p2))
  #                     #set($typ3 = $val3.getClass().getName())
  #                     ## "xxxx$p3" : "$typ3",
  #                     "$p3" : "$util.escapeJavaScript($val3)"
  #                     #if($foreach.hasNext),#end
  #                   #end
  #               }
  #               #if($foreach.hasNext),#end
  #             #else
  #               "$p2" : "$util.escapeJavaScript($val2)"
  #               #if($foreach.hasNext),#end
  #             #end
  #           #end
  #         }
  #         #if($foreach.hasNext),#end
  #       #else
  #         "$p1" : "$util.escapeJavaScript($val)"
  #         #if($foreach.hasNext),#end
  #       #end
  #     #end
  #   },
  #   "stage-variables" : {
  #     #foreach($key in $stageVariables.keySet())
  #     "$key" : "$util.escapeJavaScript($stageVariables.get($key))"
  #         #if($foreach.hasNext),#end
  #     #end
  #   },
  #   "context" : {
  #       "account-id" : "$context.identity.accountId",
  #       "api-id" : "$context.apiId",
  #       "api-key" : "$context.identity.apiKey",
  #       "authorizer-principal-id" : "$context.authorizer.principalId",
  #       "caller" : "$context.identity.caller",
  #       "cognito-authentication-provider" : "$context.identity.cognitoAuthenticationProvider",
  #       "cognito-authentication-type" : "$context.identity.cognitoAuthenticationType",
  #       "cognito-identity-id" : "$context.identity.cognitoIdentityId",
  #       "cognito-identity-pool-id" : "$context.identity.cognitoIdentityPoolId",
  #       "http-method" : "$context.httpMethod",
  #       "stage" : "$context.stage",
  #       "source-ip" : "$context.identity.sourceIp",
  #       "user" : "$context.identity.user",
  #       "user-agent" : "$context.identity.userAgent",
  #       "user-arn" : "$context.identity.userArn",
  #       "request-id" : "$context.requestId",
  #       "resource-id" : "$context.resourceId",
  #       "resource-path" : "$context.resourcePath"
  #   }
  # }

}
