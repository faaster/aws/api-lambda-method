output "type" {
  value = "${aws_api_gateway_integration.this.http_method}${var.resource_path}"
}

output "functions" {
  description = "The list of per-stage Lambda function information."
  value = {
    for k, v in local.fn_stage_map : k => merge(v, {
      lambda = {
        version = aws_lambda_function.this[k].version
        name    = aws_lambda_function.this[k].function_name
        arn     = aws_lambda_function.this[k].arn
      }
    })
  }
}

output "trigger_hash" {
  description = "Hash of aspects of the method spec that if changed should be used to trigger re-deployment."
  value = sha512(jsonencode([
    aws_api_gateway_integration_response.this_200.response_templates,
    aws_api_gateway_method_response.this_200.response_models,
    aws_api_gateway_integration.this.id,
    aws_api_gateway_integration.this.request_templates,
    aws_api_gateway_method.this.id
  ]))
}
